<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Terminals extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('terminals', function (Blueprint $table) {
			$table->increments('id');
			$table->text('name');
			$table->text('address');
			$table->char('lat', 50);
			$table->char('lon', 50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('terminals');
	}
}
