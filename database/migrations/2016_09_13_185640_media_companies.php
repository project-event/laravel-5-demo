<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MediaCompanies extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('media_companies', function($table) {
			$table->increments('id')->unsigned();
			$table->string('name', 200);
			$table->dateTime('start_date');
			$table->dateTime('end_date')->nullable();
			$table->tinyInteger('is_active');
			$table->timestamps();
		});
		
		Schema::create('content', function($table) {
			$table->increments('id')->unsigned();
			$table->integer('media_company_id')->unsigned();
			$table->enum('type', ['image']);
			$table->string('path', 200);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('media_companies');
		Schema::drop('content');
	}
}
