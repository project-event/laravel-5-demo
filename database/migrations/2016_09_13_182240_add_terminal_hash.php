<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTerminalHash extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('terminals', function(Blueprint $table) {
			$table->string('hash', 200);
			$table->index('hash');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('terminals', function(Blueprint $table) {
			$table->dropColumn('hash');
		});
	}
}
