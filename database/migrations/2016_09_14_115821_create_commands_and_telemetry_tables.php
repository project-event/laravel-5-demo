<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsAndTelemetryTables extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('commands', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('terminal_id')->default(0);
			$table->string('device')->default('');
			$table->string('value')->default('');
			$table->timestamps();
			//
			$table->index('terminal_id');
		});

		Schema::create('commands_registry', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('terminal_id')->default(0);
			$table->unsignedInteger('command_id')->default(0);
			$table->string('device')->default('');
			$table->string('value')->default('');
			$table->boolean('passed')->default(0);
			$table->timestamps();
			//
			$table->index('terminal_id');
			$table->index('device');
			$table->index('passed');
		});

		Schema::create('telemetry', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('terminal_id')->default(0);
			$table->string('device')->default('');
			$table->string('value')->default('');
			$table->timestamps();
			//
			$table->index('terminal_id');
		});

		Schema::create('telemetry_registry', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('terminal_id')->default(0);
			$table->unsignedInteger('telemetry_id')->default(0);
			$table->string('device')->default('');
			$table->string('value')->default('');
			$table->timestamps();
			//
			$table->index('terminal_id');
			$table->index('device');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('commands');
		Schema::drop('commands_registry');
		Schema::drop('telemetry');
		Schema::drop('telemetry_registry');
	}
}
