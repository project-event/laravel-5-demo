<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->command->info('DatabaseSeeder started');
		$this->call(UsersSeeder::class);
		$this->call(TerminalsSeeder::class);
		$this->call(TelemetrySeeder::class);
		$this->call(CommandsSeeder::class);
		$this->call(ContentSeeder::class);
	}
}
