<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// admin, catch

		$u = new \App\User();
		$u->id = 1;
		$u->name = 'Админ Админов';
		$u->login = 'admin';
		$u->email = 'admin@email.some';
		$u->password = Hash::make(strval('admin'));
		$u->save();
		$this->command->info('   admin added');

		$u = new \App\User();
		$u->id = 2;
		$u->name = 'Максим Кирюшкин';
		$u->login = 'catch';
		$u->email = 'catch@mail.ru';
		$u->password = Hash::make(strval('123'));
		$u->save();
		$this->command->info('   catch added');
	}
}
