<?php

use Illuminate\Database\Seeder;

class TerminalsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// test01
		$term = new \App\Terminal();
		$term->id = 1;
		$term->name = 'Тестовый терминал';
		$term->address = 'Чичерина, 37';
		$term->lat = 55.174418;
		$term->lon = 61.303542;
		$term->hash = 'test01';
		$term->save();
		$this->command->info('   test01 added');
	}
}
