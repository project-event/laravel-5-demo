<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// media_company
		// cityphoto01.jpg, cityphoto02.jpg, cityphoto03.jpg, cityphoto04.jpg

		$mc = new \App\MediaCompany();
		$mc->id = 1;
		$mc->name = 'City views';
		$mc->start_date = '2016-09-15';
		$mc->end_date = NULL;
		$mc->is_active = 1;
		$mc->save();
		$this->command->info('   media company 1 added');

		$photo = new \App\Content();
		$photo->media_company_id = $mc->id;
		$photo->type = 'image';
		$photo->path = '/uploads/cityphoto01.jpg';
		$photo->save();
		$this->command->info('   media company 1: cityphoto01.jpg added');

		$photo = new \App\Content();
		$photo->media_company_id = $mc->id;
		$photo->type = 'image';
		$photo->path = '/uploads/cityphoto02.jpg';
		$photo->save();
		$this->command->info('   media company 1: cityphoto02.jpg added');

		$photo = new \App\Content();
		$photo->media_company_id = $mc->id;
		$photo->type = 'image';
		$photo->path = '/uploads/cityphoto03.jpg';
		$photo->save();
		$this->command->info('   media company 1: cityphoto03.jpg added');

		$photo = new \App\Content();
		$photo->media_company_id = $mc->id;
		$photo->type = 'image';
		$photo->path = '/uploads/cityphoto04.jpg';
		$photo->save();
		$this->command->info('   media company 1: cityphoto04.jpg added');
	}
}
