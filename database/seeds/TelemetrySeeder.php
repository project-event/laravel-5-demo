<?php

use Illuminate\Database\Seeder;

class TelemetrySeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// test01 --> lamp1:1 -- 2 days ago
		// test01 --> temp1:16.28 -- 1 day ago
		// test01 --> temp1:18.04 -- 2 hours ago

		// - test01
		$term = \App\Terminal::whereHash('test01')->first();
		if (!$term) {
			$this->command->info('   terminal test01 doesn\'t exists!');
			return;
		}// if no terminal ...

		// test01 --> lamp1:1 -- 2 days ago
		$tel = new \App\Telemetry();
		$tel->terminal_id = $term->id;
		$tel->device = 'lamp1';
		$tel->value = '1';
		$tel->created_at = \Carbon\Carbon::now()->subDays(2);
		$tel->updated_at = $tel->created_at;
		$tel->save();
		$this->command->info('   added telemetry: test01 --> lamp1:1 at '.$tel->created_at->toDateTimeString());

		// test01 --> temp1:16.28 -- 1 day ago
		$tel = new \App\Telemetry();
		$tel->terminal_id = $term->id;
		$tel->device = 'temp1';
		$tel->value = '16.28';
		$tel->created_at = \Carbon\Carbon::now()->subDays(1);
		$tel->updated_at = $tel->created_at;
		$tel->save();
		$this->command->info('   added telemetry: test01 --> temp1:16.28 at '.$tel->created_at->toDateTimeString());

		// test01 --> temp1:18.04 -- 2 hours ago
		$tel = new \App\Telemetry();
		$tel->terminal_id = $term->id;
		$tel->device = 'temp1';
		$tel->value = '18.04';
		$tel->created_at = \Carbon\Carbon::now()->subHours(2);
		$tel->updated_at = $tel->created_at;
		$tel->save();
		$this->command->info('   added telemetry: test01 --> temp1:18.04 at '.$tel->created_at->toDateTimeString());
	}
}
