<?php

use Illuminate\Database\Seeder;

class CommandsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// test01 --> lamp1: 1
		// - test01
		$term = \App\Terminal::whereHash('test01')->first();
		if (!$term) {
			$this->command->info('   terminal test01 doesn\'t exists!');
			return;
		}// if no terminal ...

		$cmd = new \App\Command();
		$cmd->terminal_id = $term->id;
		$cmd->device = 'lamp1';
		$cmd->value = '1';
		$cmd->save();

		$this->command->info('   added command: test01 --> lamp1:1');
	}
}
