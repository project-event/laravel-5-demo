<?php

// see: http://stackoverflow.com/a/28290359
// see: http://laravel.io/forum/02-03-2015-best-practices-for-custom-helpers-on-laravel-5?page=1

/**
 * Ищет в манифесте подходящий по имени файл, возвращает путь с учётом $path
 * и номером версии, который был добавлен при сборке файла в gulp. В файле
 * манифеста перечислены соответствия имён файлов с их текущими актуальными
 * версиями.
 *
 * Если файл не найден в манифесте, подставляется $filename.
 *
 * Если $path не указан, он подбирается автоматически по расширению файла:
 *
 *    *.css --> /css/
 *    *.js --> /js/
 *    *.* --> /
 *
 * Пример использования:
 *
 *    <link href="{{ revisioned('test/all.css') }}" rel="stylesheet" type="text/css">
 *    заменяется на
 *    <link href="/css/test/all-165a85eeec.css" rel="stylesheet" type="text/css">
 *
 * @param string $filename Имя файла для поиска в манифесте
 * @param string|null $path Путь к конечному файлу при выдаче браузеру (как правило именно браузеру)
 * @param string $manifestFile Полный путь к файлу с манифестом относительно корневой директории проекта
 * @return string
 */
function revisioned($filename, $path = NULL, $manifestFile = 'rev-manifest.json') {
	if ($path === NULL) {
		if (StrEndsWith($filename, '.css')) $path = '/css/';
		elseif (StrEndsWith($filename, '.js')) $path = '/js/';
		else $path = '/';
	}// if autoset prefix ...
	$manifestPath = base_path().DIRECTORY_SEPARATOR.$manifestFile;
	// небольшое кэширование через глобальную переменную
	global $revisionedManifests;
	if (isset($revisionedManifests[$manifestPath])) {
		$manifest = $revisionedManifests[$manifestPath];
	} else {
		$manifest = file_exists($manifestPath) ? json_decode(file_get_contents($manifestPath), TRUE) : [];
		$revisionedManifests[$manifestPath] = $manifest;
	}// if cached ... else load ...
	return $path.(array_key_exists($filename, $manifest) ? $manifest[$filename] : $filename);
}// revisioned()

/**
 * Экранирует спецсимволы, чтобы в html текст вставился без тегов
 *
 * @param string $source Исходные текст
 * @return string
 */
function ToHTML($source) {
	return htmlspecialchars($source, ENT_QUOTES);
}// ToHTML()

/**
 * Переводит строку в число с точкой, даже если разделителем была запятая
 *
 * @param string $str Строка, содержащая число с плавающей точкой - разделитель точка или запятая
 * @return double
 */
function ToDouble($str) {
	$str = preg_replace('/[^\d-+\.\,]/', '', mb_trim($str));
	$str = preg_replace('/^(.*?)(\-?\d+)[\.\,]?(\d*)(.*?)$/', '$2.$3', $str);
	return doubleval($str);
}// ToDouble()

if (!function_exists('mb_trim')) {
	/**
	 * Юникодный trim, возвращает строку без лишних символов в начале и в конце
	 *
	 * @param string $str Исходный текст
	 * @param string $charlist Неугодный символ или строка - регулярное выражение
	 * @return string
	 */
	function mb_trim($str, $charlist = '\\s') {
		if (!mb_strlen($str)) return false;
		$symbols = '[' . $charlist . ']+';
		$pattern = '^' . $symbols . '|' . $symbols . '$';
		return preg_replace("/$pattern/usD", '', $str);
	}// mb_trim()
}// if no mb_trim() ...

/**
 * Определяет, является ли testpart началом строки string
 *
 * @param string $str Исходная тестируемая строка
 * @param string $testpart Кусок, который ищем в начале строки
 * @return bool
 * @see http://stackoverflow.com/a/834355
 */
function StrStartsWith($str, $testpart) {
	$length = mb_strlen($testpart);
	return (mb_substr($str, 0, $length) === $testpart);
}// StrStartsWith()

/**
 * Определяет, является ли testpart хвостиком строки string
 *
 * @param string $str Исходная тестируемая строка
 * @param string $testpart Кусок, который ищем в конце строки
 * @return bool
 * @see http://stackoverflow.com/a/834355
 */
function StrEndsWith($str, $testpart) {
	$length = mb_strlen($testpart);
	if ($length == 0) return true;
	$start = $length * -1; // negative
	return (mb_substr($str, $start) === $testpart);
}// StrEndsWith()
