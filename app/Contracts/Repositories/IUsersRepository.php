<?php

namespace App\Contracts\Repositories;

interface  IUsersRepository {
	public function getUsersList($ajax = false);
	public function getUserById($id);
	public function deleteUser();
	public function createOrUpdate();
}
