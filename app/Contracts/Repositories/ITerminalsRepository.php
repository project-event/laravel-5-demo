<?php

namespace App\Contracts\Repositories;

interface  ITerminalsRepository {
	public function getTerminalsList($ajax = false);
	public function getTerminalById($id);
	public function deleteTerminal();
	public function createOrUpdate();
	public function getTerminalsListMap();
}
