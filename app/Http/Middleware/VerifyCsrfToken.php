<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {
	/**
	 * The URIs that should be excluded from CSRF verification.
	 *
	 * @var array
	 */
	protected $except = [
		'/login',
		'/users/DocSave',
		'/users',
		'/users/Delete',
		'/terminals/DocSave',
		'/terminals',
		'/terminals/Delete',
		'/terminals/getMapData',
		'/terminals/paramList',
		'/terminals/SetParamByAction'
	];
}
