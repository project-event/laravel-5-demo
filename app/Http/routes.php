<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect()->guest('terminals');
});

Route::auth();

Route::get('register', function () {
	return redirect()->guest('users');
});

// users
Route::get('/users', ['middleware' => 'auth', 'uses' => 'UsersController@index']);
Route::post('/users', ['middleware' => 'auth', 'uses' => 'UsersController@ListAjax']);
Route::get('users/{id}', ['middleware' => 'auth', 'uses' => 'UsersController@index']);
Route::post('/users/Delete', ['uses' => 'UsersController@Delete']);
Route::post('/users/DocSave', ['uses' => 'UsersController@DocSave']);

// terminals
Route::get('/terminals', ['middleware' => 'auth', 'uses' => 'TerminalsController@index']);
Route::post('/terminals', ['middleware' => 'auth', 'uses' => 'TerminalsController@ListAjax']);
Route::get('/terminals/map', ['middleware' => 'auth', 'uses' => 'TerminalsController@getMap']);
Route::get('/terminals/{id}', ['middleware' => 'auth', 'uses' => 'TerminalsController@index']);
Route::post('/terminals/Delete', ['uses' => 'TerminalsController@Delete']);
Route::post('/terminals/DocSave', ['uses' => 'TerminalsController@DocSave']);
Route::post('/terminals/getMapData', ['uses' => 'TerminalsController@GetTerminalsMapData']);
Route::post('/terminals/SetParamByAction', ['uses' => 'TerminalsController@SetParamByAction']);
Route::post('/terminals/paramList', ['uses' => 'TerminalsController@ParamsListAjax']);
