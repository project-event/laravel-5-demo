<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

use App\Contracts\Repositories\ITerminalsRepository;

class TerminalsController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(ITerminalsRepository $terminal) {
		$this->terminal = $terminal;
	}

	public function index($id = null) {
		$DATA = [];
		if ($id === null || $id === '') {
			$list = $this->terminal->getTerminalsList();
			
			$result = array('Items' => $list ?: array());
			
			$VIEW = view('terminal.list', $result);
		} else {
			$item = $this->terminal->getTerminalById(intval($id));
			$backUrl = strval(URL::previous());
			
			$result = array('Item' => $item ?: array(), 'BackUrl' => $backUrl ?: '');
			
			$VIEW = view('terminal.doc', $result);
		}

		return $VIEW;
	}
	
	public function ListAjax() {
		$list = $this->terminal->getTerminalsList(true);

		$VIEW = view('terminal.terminal-list-only', Array('Items' => $list));
		if (is_string($list)) {
			return json_encode(array('status' => 'error', 'reason' => $list));
		} else {
			return json_encode(array('status' => 'success', 'content' => strval($VIEW)));
		}
	}

	public function Delete () {
		$res = $this->terminal->deleteTerminal();

		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {
			return json_encode(array('status' => 'success'));
		}
	}

	public function DocSave () {
		$res = $this->terminal->createOrUpdate();
		
		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {
			return json_encode(array('status' => 'success'));
		}
	}
	
	public function getMap () {
		$result = array('map' => true);
			
		$VIEW = view('terminal.list', $result);
		
		return $VIEW;
	}
	
	public function GetTerminalsMapData () {
		$res = $this->terminal->getTerminalsListMap();
		
		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {
			return json_encode(array('status' => 'success', 'data' => $res));
		}
	}
	
	public function ParamsListAjax() {
		$list = array(
			array(  'code' => 'flap',
				'name' => 'Заслонка',
				'state' => 'Окрыта',
				'lastMoment' => '2016-09-15 02:37:04',
				'actions' => array(
					'close' => array('val' => 'close', 'name' => 'Закрыто', 'set' => true),
					'open' => array('val' => 'open', 'name' => 'Открыта', 'set' => false),
					'auto' => array('val' => 'auto', 'name' => 'Автоматически', 'set' => false),
				)
			)
		);
		
		if (is_string($list)) {
			return json_encode(array('status' => 'error', 'reason' => $list));
		} else {
			$result = array('Params' => $list);
			$VIEW = view('terminal.paramsTable-list-only', Array('Item' => $result));
			
			return json_encode(array('status' => 'success', 'content' => strval($VIEW)));
		}
	}
	
	public function SetParamByAction(){
		$res = true;
	    
		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {		
			return json_encode(array('status' => 'success'));
		}
	}

}
