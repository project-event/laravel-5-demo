<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

use App\Contracts\Repositories\IUsersRepository;

class UsersController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(IUsersRepository $user){
		$this->user = $user;
	}

	public function index($id = null) {
		$DATA = [];
		if ($id === null || $id === '') {
			$list = $this->user->getUsersList();
			
			$result = array('Items' => $list ?: array());
			
			$VIEW = view('user.list', $result);
		} else {
			$item = $this->user->getUserById(intval($id));
			$backUrl = strval(URL::previous());
			
			$result = array('Item' => $item ?: array(), 'BackUrl' => $backUrl ?: '');
			
			$VIEW = view('user.doc', $result);
		}

		return $VIEW;
	}
	
	public function ListAjax() {
		$list = $this->user->getUsersList(true);

		$VIEW = view('user.user-list-only', Array('Items' => $list));
		if (is_string($list)) {
			return json_encode(array('status' => 'error', 'reason' => $list));
		} else {
			return json_encode(array('status' => 'success', 'content' => strval($VIEW)));
		}
	}

	public function Delete () {
		$res = $this->user->deleteUser();

		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {
			return json_encode(array('status' => 'success'));
		}
	}

	public function DocSave () {
		$res = $this->user->createOrUpdate();
		
		if (is_string($res)) {
			return json_encode(array('status' => 'error', 'reason' => $res));
		} else {
			return json_encode(array('status' => 'success'));
		}
	}

}
