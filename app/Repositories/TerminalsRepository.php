<?php

namespace App\Repositories;
use Illuminate\Pagination\Paginator;

//interface
use App\Contracts\Repositories\ITerminalsRepository;

//models
use App\Terminal;

class TerminalsRepository implements ITerminalsRepository {
    
	public function getTerminalsList($ajax = false) {
		
		if ($ajax && isset($_REQUEST['page']) && $_REQUEST['page']) {
			$currentPage = $_REQUEST['page'];
			Paginator::currentPageResolver(function () use ($currentPage) {
				return $currentPage;
			});
		}
		
		$list = Terminal::paginate(15);
		
		return $list ? $list : array();
	}
	
	public function getTerminalsListMap() {
		$list = Terminal::all();
		
		if (!$list)
			return 'Не удалось получить список киосков, попробуйте позже';
		
		$DATA = array();
		foreach ($list as $l) {
			if (!$l->id && (($l->lat != null) || ($l->lon != null)))
				break;
		    
			$terminal = array();
			$terminal['id'] = intval($l->id);
			$terminal['name'] = $l->name ? strval($l->name) : '';
			$terminal['address'] = $l->address ? strval($l->address) : '';
			$terminal['lat'] = floatval($l->lat);
			$terminal['lon'] = floatval($l->lon);
		    
			$DATA[] = $terminal;
		}
		
		return $DATA ? $DATA : array();
	}

	public function getTerminalById($id) {
		$item = Terminal::find($id);

		return $item ? $item : array();
	}

	public function deleteTerminal() {
		$id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

		if ($id) {
			Terminal::destroy($id);
		} else
			return 'Нельзя удалить запись, которая ещё не сохранена';

		return true;
	}

	public function createOrUpdate() {	
		$id = intval($_REQUEST['id']) ?: 0;
		
		if ($id) {
			$terminal = Terminal::find($id);

			if (!$terminal)
				return 'Не удалось найти запись в базе данных. Попробуйте выполнить операцию с самого начала.';
		} else
			$terminal = new Terminal;

		$errors = array();
		if (!$_REQUEST['name']) $errors[] = "Заполниет поле 'Пароль', пожалуйста";
		if (!$_REQUEST['hash']) $errors[] = "Заполниет поле 'Хеш', пожалуйста";
		if (!$_REQUEST['lat'] || !$_REQUEST['lon']) $errors[] = "Координаты обязательны для заполнения";
		
		if ($errors) return implode('; ', $errors);

		$terminal->name = $_REQUEST['name'] ? strval($_REQUEST['name']) : '';
		$terminal->address = $_REQUEST['address'] ? strval($_REQUEST['address']) : '';
		$terminal->lat = $_REQUEST['lat'] ? ToDouble(strval($_REQUEST['lat'])) : '';
		$terminal->lon = $_REQUEST['lon'] ? ToDouble(strval($_REQUEST['lon'])) : '';
		$terminal->hash = $_REQUEST['hash'] ? strval($_REQUEST['hash']) : '';
		
		$terminal->save();

		return true;
	}

}// class TerminalsRepository
