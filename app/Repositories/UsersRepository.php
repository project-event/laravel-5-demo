<?php

namespace App\Repositories;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;

//interface
use App\Contracts\Repositories\IUsersRepository;

//models
use App\User;

class UsersRepository implements IUsersRepository {
    
	public function getUsersList($ajax = false) {
		
		if ($ajax && isset($_REQUEST['page']) && $_REQUEST['page']) {
			$currentPage = $_REQUEST['page'];
			Paginator::currentPageResolver(function () use ($currentPage) {
				return $currentPage;
			});
		}
		
		$list = User::paginate(15);
		
		return $list ? $list : array();
	}

	public function getUserById($id) {
		$item = User::find($id);

		return $item ? $item : array();
	}

	public function deleteUser() {
		$id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

		if ($id) {
			User::destroy($id);
		} else
			return 'Нельзя удалить запись, которая ещё не сохранена';

		return true;
	}

	public function createOrUpdate() {	
		$id = intval($_REQUEST['id']) ?: 0;
		
		if ($id) {
			$user = User::find($id);

			if (!$user)
				return 'Не удалось найти запись в базе данных. Попробуйте выполнить операцию с самого начала.';
		} else
			$user = new User;

		$errors = array();
		if (!$_REQUEST['name']) $errors[] = "Заполните поле 'Наименование', пожалуйста";
		if (!$_REQUEST['login']) $errors[] = "Заполните поле 'Логин', пожалуйста";
		if (!$_REQUEST['email']) $errors[] = "Заполниете поле 'E-Mail', пожалуйста";
		//if (!$_REQUEST['password']) $errors[] = "Заполниет поле 'Пароль', пожалуйста";
		$u = User::where('email', '=', strval(trim($_REQUEST['email'])))->first();
		if ($u && $u->id != $id)
			$errors[] = "Пользователь с таким email уже зарегистрирован";
		
		if ($errors) return implode('; ', $errors);

		$user->name = $_REQUEST['name'] ? strval($_REQUEST['name']) : '';
		$user->login = $_REQUEST['login'] ? strval($_REQUEST['login']) : '';
		$user->email = $_REQUEST['email'] ? strval($_REQUEST['email']) : '';
		
		//если поле пароль не заднно не изменять его, нельзя задать пустой пароль
		if (isset($_REQUEST['password']) && $_REQUEST['password'])
			$user->password = Hash::make(strval($_REQUEST['password']));
		
		$user->save();

		return true;
	}

}// class UsersRepository
