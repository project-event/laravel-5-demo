<?php

namespace App\Providers;

//use App\Command;
//use App\Telemetry;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		error_reporting(E_ALL ^ E_NOTICE);
		mb_internal_encoding('UTF-8');
		mb_regex_encoding('UTF-8');

//		Telemetry::saved(function (Telemetry $it) {
//			$it->updateRegistry();
//		});
//		Command::saved(function (Command $it) {
//			$it->updateRegistry();
//		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		// instruments
		$this->app->bind(
			'App\Contracts\Repositories\IUsersRepository',
			'App\Repositories\UsersRepository'
		);
		$this->app->bind(
			'App\Contracts\Repositories\ITerminalsRepository',
			'App\Repositories\TerminalsRepository'
		);
	}
}
