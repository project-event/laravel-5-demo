<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Terminal
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $lat
 * @property string $lon
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $hash
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereLat($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereLon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terminal whereHash($value)
 * @mixin \Eloquent
 */
class Terminal extends Model {
	protected $fillable = [
		'name', 'address', 'lat', 'lon', 'hash'
	];
}
