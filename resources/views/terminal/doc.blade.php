@extends('layouts.app')

@section('content')
<script>
$(function () {
	UpdateParamsTable();
	setInterval(UpdateParamsTable, 20000);
	
	$('#terminal-params').on('change', 'td.lst-actions-item div.radio', function () {
		var actionCode = $(this).closest('tr').data('itemid');
		var actionType = $(this).find('input:checked').val();

		ActionSend(actionCode, actionType);
		return false;
	});
	
        $('#save').on('click', function () {
		var data = {
			id: window.terminalId,
			name: $('#name').val(),
			hash: $('#hash').val(),
			address: $('#address').val(),
			lat: $('#lat').val(),
			lon: $('#lon').val(),
		};
		
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.docSaveURL,
			dataType: 'json',
			data: data,
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				alert('Запись успешно сохранена');
				setTimeout(function () {
					window.location.href = window.docSuccessURL;
				}, 0);
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте повторить операцию позже');
			}// error()
		});// ajax()
	});
	
	function ActionSend(actionCode, actionType) {
		var data = {
			tId: window.terminalId,
			aCode: actionCode,
			aType: actionType
		};
	    
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.setParamByActionURL,
			dataType: 'json',
			data: data,
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				alert('Параметр успешно установлен');
				UpdateParamsTable();
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте повторить операцию позже');
			}// error()
		});// ajax()
	}
	
	function UpdateParamsTable() {
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.updateParamListURL,
			dataType: 'json',
			data: {
			    tId: window.terminalId
			},
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				$("#params-list").remove();
				$('#terminal-params').append(D.content);
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте выполнить обновление странице позже');
			}// error()
		});// ajax()
	}
});
</script>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
	    <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ isset($BackUrl) ? $BackUrl : "/terminals" }}">Киоски</a>&nbsp;/&nbsp;{{ (isset($Item['id']) && $Item['id']) ? 'Редактирование киоска' : 'Добавление киоска' }}</div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Наименование</label>
                            <div class="col-md-7">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($Item['name']) ? $Item['name'] : '' }}">
                            </div>
                        </div>
			<div class="form-group">
                            <label for="hash" class="col-md-3 control-label">Хеш</label>
                            <div class="col-md-7">
                                <input id="hash" type="text" class="form-control" name="hash" value="{{ isset($Item['hash']) ? $Item['hash'] : '' }}">
                            </div>
                        </div>
			<div class="form-group">
                            <label for="address" class="col-md-3 control-label">Адресс</label>
                            <div class="col-md-7">
				<textarea id="address" type="text" class="form-control" rows="3" name="address">{{ isset($Item['address']) ? $Item['address'] : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lat" class="col-md-3 control-label">Lat</label>
                            <div class="col-md-3">
                                <input id="lat" type="string" class="form-control" name="lat" value="{{ isset($Item['lat']) ? $Item['lat'] : '' }}">
                            </div>
			    <label for="lon" class="col-md-1 control-label">Lon</label>
                            <div class="col-md-3">
                                <input id="lon" type="string" class="form-control" name="lon" value="{{ isset($Item['lon']) ? $Item['lon'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-3">
                                <button id="save" type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
		    <script type="text/javascript">
			window.terminalId = {{ isset($Item['id']) ? $Item['id'] : 0 }};
			window.docSaveURL = '/terminals/DocSave';
			window.docSuccessURL = '{{ isset($BackUrl) ? $BackUrl : "/terminals" }}';
			window.setParamByActionURL = '/terminals/SetParamByAction';
			window.updateParamListURL = '/terminals/paramList';
		    </script>
                </div>
            </div>
	    <div id="terminal-params">
	    @if (isset($Item['Params']))
		@include('terminal.paramsTable-list-only', $Item)
	    @endif
	    {{-- @include('terminal.paramsTable-list-only', $Item) --}}
	    </div>
	</div>
    </div>
</div>
@endsection  
