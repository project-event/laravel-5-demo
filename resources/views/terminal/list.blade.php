@extends('layouts.app')
@section('content')
<script>
$(function () {
	$('#list-content').on('click', 'td.lst-actions-item a.deleteItem', function () {
		this.blur();
		window.removingListItemId = $(this).closest('tr').data('itemid');
		
		var answer = confirm("Удалить киоск " + window.removingListItemId + "?")
		if (answer){
		       DeleteAction();
		}
		
		return false;
	});
	
	function UpdateList() {
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.updateListURL,
			dataType: 'json',
			data: {
			    page: window.currentPage
			},
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				$("#terminal-list").remove();
				$('#list-content').append(D.content);
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте выполнить обновление странице позже');
			}// error()
		});// ajax()
	}
    
	function DeleteAction() {
		var data = {
			id: window.removingListItemId
		};
	    
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.removingListURL,
			dataType: 'json',
			data: data,
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				alert('Запись успешно удалена');
				UpdateList();
				/*setTimeout(function () {
					window.location.reload();
				}, 50);*/
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте повторить операцию позже');
			}// error()
		});// ajax()
	}
});
</script>
<style>
    .title > h3, .title > a {
	display: inline-block;
	margin-bottom: 10px!important;
	margin-right: 10px!important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1" id="list-content">
	    <div class="title">
		<h3>Киоски</h3>
		<a href="/terminals/0" class="btn btn-primary btn-sm"
		data-toggle="tooltip" data-placement="bottom"
		title="Добавить киоск">
		<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Добавить</a>
		@if (!isset($map) || !$map)
		<a href="/terminals/map" class="btn btn-primary btn-sm"
		data-toggle="tooltip" data-placement="bottom"
		title="На карте">
		<span class="glyphicon glyphicon-map-marker"></span>&nbsp;&nbsp;На карте</a>
		@endif
	    </div>
	    @if (!isset($map) || !$map)
		@include('terminal.terminal-list-only', $Items)
	    @else
		@include('terminal.terminal-map')
	    @endif
        </div>
    </div>
</div> 
@endsection