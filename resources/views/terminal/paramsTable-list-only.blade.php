<div id="params-list">
    <h4>Переферия</h4>
    <table class="table table-striped table-hover table-list">
	<thead>
	<tr>
	    <th class="lst-name-header">
		Наименование
	    </th>
	    <th class="col-sm-3 lst-state-header">
		Статус
	    </th>
	    <th class="col-sm-3 lst-last-update-header">
		Последнее изменение
	    </th>
	    <th class="col-sm-1 lst-actions-header">
		Действия
	    </th>
	</tr>
	</thead>
	@if (isset($Item['Params']) && $Item['Params'])
	    @foreach ($Item['Params'] as $p)
		<tr data-itemid="{{ isset($p['code']) ? $p['code'] : '' }}">
		    <td class="lst-id-item">{{ isset($p['name']) ? $p['name'] : '' }}</td>
		    <td class="lst-name-item">{{ isset($p['state']) ? $p['state'] : '' }}</td>
		    <td class="lst-name-item">{{ isset($p['lastMoment']) ? $p['lastMoment'] : '' }}</td>
		    <td class="lst-actions-item">
			@if (isset($p['actions']) && $p['actions'])
			    @foreach ($p['actions'] as $a)
			    <div class="radio">
				<label>
				    <input type="radio" name="optionsRadios" class="change-mode" {{ (isset($a['set']) && $a['set']) ? 'checked' : '' }} value="{{ isset($a['val']) ? $a['val'] : '' }}">
				    {{ isset($a['name']) ? $a['name'] : '' }}
				</label>
			    </div>
			    @endforeach
			@endif
		    </td>
		</tr>
	    @endforeach
	@endif
    </table>
</div>
