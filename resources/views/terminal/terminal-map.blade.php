<div class="panel panel-default">
    <div class="panel-heading"><a href="/terminals">Киоски</a>&nbsp;/&nbsp;Киоски на карте</div>
    <div class="panel-body">
	<div id="terminal-map"></div>
    </div>
</div>
<style>
    #terminal-map {
	width: 100%;
	height: 450px;
    }
</style>
<script>
	var map;
	var markers = [];
	
	function initMap() {
		var mapDiv = document.getElementById('terminal-map');
		map = new google.maps.Map(mapDiv, {
			center: {lat: 55.154, lng: 61.429},
			zoom: 11
		});
		getMapMarkers();
	}
    
    	function getMapMarkers() {
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.getMapDataUrl,
			dataType: 'json',
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				
				if (D.data) {
					D.data.forEach(function(data) {
						var marker = new google.maps.Marker({
							position: {lat: data.lat, lng: data.lon},
							map: map,
							title: data.name 
						});
						
						var popUp = CreateInfoWindow(data.id, data.name, data.address);
						google.maps.event.addListener(marker, 'click', function() {
							popUp.open(map, marker);
						});
						
						markers.push(marker);
					});
				}
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте позже');
			}// error()
		});// ajax()
	}
	
	function CreateInfoWindow(id, name, address) {
		var contentString = '<div id="content"><h5>'+name+'<h5><p>Адресс: '+address+'</p><a href="/terminals/'+id+'">редактировать</a></div>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		
		return infowindow;
	}
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRuTyWyFOmPzc6NLg-2zZTOY9E2yr6vv4&callback=initMap">
</script>
<script>
    window.getMapDataUrl = '/terminals/getMapData';
</script>