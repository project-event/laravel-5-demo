<style>
    .list-actions {
	white-space:nowrap;
	overflow:auto;
    }
    .list-actions div {
	display:inline-block;
    }
</style>
<div id="user-list">
    <table class="table table-striped table-hover table-list">
	<thead>
	<tr>
	    <th class="col-sm-1 lst-id-header">
		id
	    </th>
	    <th class="lst-name-header">
		Имя
	    </th>
	    <th class="col-sm-4 lst-tasks-techop-header">
		Логин
	    </th>
	    <th class="col-sm-1 lst-actions-header">
		Действия
	    </th>
	</tr>
	</thead>
	@foreach ($Items as $item)
	    <tr data-itemid="{{ isset($item->id) ? $item->id : '' }}">
		<td class="lst-id-item">{{ isset($item->id) ? $item->id : '' }}</td>
		<td class="lst-name-item">
		    <a href="/users/{{ isset($item->id) ? $item->id : '' }}">{{ isset($item->name) ? $item->name : '' }}</a>
		</td>
		<td class="lst-login-item">{{ isset($item->login) ? $item->login : '' }}</td>
		<td class="lst-actions-item">
		    <div class="row">
			<div class="list-actions">
			    <div class="col-sm-6">
				<a href="/users/{{ isset($item->id) ? $item->id : '' }}" class="editItem" data-toggle="tooltip" data-placement="top" title="Редактировать">
				    <span class="glyphicon glyphicon-pencil"></span>
				</a>
			    </div>
			    <div class="col-sm-6">
				<a href="#delete" class="deleteItem" data-toggle="tooltip" data-placement="top" title="Удалить">
				    <span class="glyphicon glyphicon-trash"></span>
				</a>
			    </div>
			<div>
		    </div>
		</td>
	    </tr>
	@endforeach
    </table>
    <script>
	window.removingListURL = '/users/Delete';
	window.updateListURL = '/users';
	window.currentPage = {{ $Items->currentPage() }};
    </script>
    {{ $Items->render() }}
</div>

