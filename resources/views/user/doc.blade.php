@extends('layouts.app')

@section('content')
<script>
$(function () {
        $('#save').on('click', function () {
		var data = {
			id: window.userId,
			name: $('#name').val(),
			login: $('#login').val(),
			email: $('#email').val(),
			password: $('#password').val(),
		};
		
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.docSaveURL,
			dataType: 'json',
			data: data,
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				alert('Запись успешно сохранена');
				setTimeout(function () {
					window.location.href = window.docSuccessURL;
				}, 0);
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте повторить операцию позже');
			}// error()
		});// ajax()
	});
});
</script>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
	    <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ isset($BackUrl) ? $BackUrl : '/users' }}">Пользователи</a>&nbsp;/&nbsp;{{ (isset($Item['id']) && $Item['id']) ? 'Редактирование пользователя' : 'Добавление пользователя' }}</div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Имя</label>
                            <div class="col-md-7">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($Item['name']) ? $Item['name'] : '' }}">
                            </div>
                        </div>
			<div class="form-group">
                            <label for="name" class="col-md-3 control-label">Логин</label>
                            <div class="col-md-7">
                                <input id="login" type="text" class="form-control" name="name" value="{{ isset($Item['login']) ? $Item['login'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">E-Mail</label>
                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control" name="email" value="{{ isset($Item['email']) ? $Item['email'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Пароль</label>
                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-3">
                                <button id="save" type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
		    <script type="text/javascript">
			window.userId = {{ isset($Item['id']) ? $Item['id'] : 0 }};
			window.docSaveURL = '/users/DocSave';
			window.docSuccessURL = '{{ isset($BackUrl) ? $BackUrl : "/users" }}';
		    </script>
                </div>
            </div>
	</div>
    </div>
</div>
@endsection  
