@extends('layouts.app')
@section('content')
<script>
$(function () {
	$('#list-content').on('click', 'td.lst-actions-item a.deleteItem', function () {
		this.blur();
		window.removingListItemId = $(this).closest('tr').data('itemid');
		
		var answer = confirm("Удалить пользователя " + window.removingListItemId + "?")
		if (answer){
		       DeleteAction();
		}
		
		return false;
	});
	
	function UpdateList() {
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.updateListURL,
			dataType: 'json',
			data: {
			    page: window.currentPage
			},
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				$("#user-list").remove();
				$('#list-content').append(D.content);
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте выполнить обновление странице позже');
			}// error()
		});// ajax()
	}
    
	function DeleteAction() {
		var data = {
			id: window.removingListItemId
		};
	    
		$.ajax({
			async: true,	// не ждем завершения
			type: 'POST',
			url: window.removingListURL,
			dataType: 'json',
			data: data,
			cache: false,
			global: false,
			success: function(D) {
				if (D.status != 'success') {
					alert(D.reason);
					return;
				}// if error ...
				alert('Запись успешно удалена');
				UpdateList();
				/*setTimeout(function () {
					window.location.reload();
				}, 50);*/
			},// success()
			error: function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown.length ? errorThrown : 'Возникла ошибка, попробуйте повторить операцию позже');
			}// error()
		});// ajax()
	}
});
</script>
<style>
    .title>h3,.title>a {
	display: inline-block;
	margin-bottom: 10px!important;
	margin-right: 10px!important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1" id="list-content">
	    <div class="title">
		<h3>Пользователи</h3>
		<a href="/users/0" class="btn btn-primary btn-sm"
		data-toggle="tooltip" data-placement="bottom"
		title="Добавить пользователя">
		<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Добавить</a>
	    </div>
	    @if (isset($Items) && $Items)
		@include('user.user-list-only', $Items)
	    @endif
        </div>
    </div>
</div> 
@endsection